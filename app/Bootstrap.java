

import java.util.List;

import play.*;
import play.jobs.*;
import play.libs.MimeTypes;
import play.test.*;
import models.*;
import play.db.jpa.Blob;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

@OnApplicationStart
public class Bootstrap extends Job 
{ 
	public void doJob() throws FileNotFoundException
	{
		Fixtures.deleteDatabase();
		Fixtures.loadModels("data.yml");

		List<User> users = User.findAll();

		for(User u: users)
		{

			String photoName = "public/images/" + u.imageFile;
			Blob blob = new Blob ();
			blob.set(new FileInputStream(photoName), MimeTypes.getContentType(photoName));
			u.profilePicture = blob;
			u.save();

		}
	}
}