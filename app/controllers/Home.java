package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;
import utils.MessageDateComparator;
import utils.MessageFromComparator;

public class Home extends Controller {

	public static void index()
	{
		if (session.get("logged_in_userid") != null)
		{
			String userId = session.get("logged_in_userid");
			User user = User.findById(Long.parseLong(userId));

			ArrayList<Message> sortedInbox = new ArrayList<>();
			sortedInbox.addAll(user.inbox);

			ArrayList<ArrayList<Message>> conversations = new ArrayList<>();
			conversations.add(sortedInbox);
			render("Home/index.html", user, conversations);

		}
		else
		{
			Accounts.login();
		}
	}

	public static void drop(Long id)
	{
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));

		User friend = User.findById(id);

		user.unfriend(friend);
		Logger.info("Dropping " + friend.email);
		index();
	}  

	public static void byUser(User user)
	{
		String userId = session.get("logged_in_userid");
		user = User.findById(Long.parseLong(userId));

		ArrayList<Message> sortedInbox = new ArrayList<>();
		sortedInbox.addAll(user.inbox);

		Collections.sort(sortedInbox, new MessageFromComparator());

		ArrayList<ArrayList<Message>> conversations = new ArrayList<>();
		conversations.add(sortedInbox);
		render("Home/index.html", user, conversations);

	}

	public static void byConversation(User user)
	{  
		String userId = session.get("logged_in_userid");
		user = User.findById(Long.parseLong(userId));

		ArrayList<ArrayList<Message>> conversations = new ArrayList<>();
		for (Friendship f: user.friendships)

		{
			if(getConversation(user, f.targetUser).size() > 0)
			{
				conversations.add(getConversation(user, f.targetUser));
			}
		}
		render("Home/index.html", user, conversations);
	}

	static ArrayList<Message> getConversation(User user, User friend)
	{
		ArrayList<Message> conversation = new ArrayList<>();
		for(Message message : user.outbox)
		{
			if(friend == message.to)
			{ 
				conversation.add(message);

			}
		}

		for(Message message: user.inbox)
		{
			if(friend == message.from)
			{
				conversation.add(message);
			}

		}
		Collections.sort(conversation, new MessageDateComparator());
		return conversation;
	}

}