package controllers;
/**
 * @file    LeaderBoard.java
 * @brief   Class publishes users graded most socially and talkatively active
 * @version June 10 2014
 * @author  
 */

import play.*;
import play.mvc.*;
import utils.UserSocialComparator;
import utils.UserTalkativeComparator;

import java.util.*;

import models.*;


public class LeaderBoard extends Controller
{
	public static void index()
	{
		if (session.get("logged_in_userid") != null)
		{
			List<User> users = User.findAll();
			Collections.sort(users, new UserSocialComparator());
			render("leaderboard/index.html", users);

		}
		else
		{
			Accounts.login();
		}
	}


	public static void talkative(List<User> users)
	{
		users = User.findAll();
		Collections.sort(users, new UserTalkativeComparator());
		render("leaderboard/index.html", users);

	}

}
